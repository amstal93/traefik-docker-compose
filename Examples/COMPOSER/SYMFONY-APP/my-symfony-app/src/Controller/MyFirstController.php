<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyFirstController extends AbstractController
{
    #[Route('/', name: 'my_first_controller')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'Voici mon 1er controller',
            'detail' => 'Twig pack n\'est pas installé, vous avez donc un rendu json.',
            'path' => 'src/Controller/MyFirstController.php',
        ]);
    }
}
