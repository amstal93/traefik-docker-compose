# Traefik - Docker-compose

Default traefik configuration with SSL (Let's Encrypt) for docker compose file

## Traefik
### Utility :

First one, you can also follow the official documentation of [Traefik]('https://doc.traefik.io/traefik/').


## Architecture

First, Traefik compose file should be sort as one of your project.

I suggest for you and architecture by folder, with each folder for your mulitple containers (as project).

**You must have ONE TRAEFIK CONTAINER running so that your other containers can be accessed from the address you give them.**



```bash
TRAEFIK-DOCKER-COMPOSE
│   .gitignore
│   README.md
│
└───Examples
    ├───HTML-APP
    │   │   docker-compose.yml
    │   │
    │   ├───conf.d
    │   │       default.conf
    │   │
    │   ├───logs
    │   │       access.log
    │   │       error.log
    │   │
    │   └───my-html-project
    │           index.html
    │
    ├───MYSQL-APP
    │   │   docker-compose.yml
    │   │
    │   └───database
    │
    ├───MYSQL-PMA-APP
    │       docker-compose.yml
    │
    ├───PHP-APP
    │   │   docker-compose.yml
    │   │   Dockerfile
    │   │
    │   ├───conf.d
    │   │       default.conf
    │   │
    │   ├───logs
    │   │       access.log
    │   │       error.log
    │   │
    │   └───my-php-project
    │           index.php
    │
    ├───COMPOSER
    │   └───SYMFONY-APP
    │       │
    │       │   docker-compose.yml
    │       │   Dockerfile
    │       │
    │       ├───conf.d
    │       │       default.conf
    │       │
    │       ├───logs
    │       │       access.log
    │       │       error.log
    │       └───my-symfony-app
    │
    └───TRAEFIK
        │   docker-compose.yml
        │
        └───letsencrypt
                acme.json
```




